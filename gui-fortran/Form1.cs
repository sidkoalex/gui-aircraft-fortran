﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace gui_fortran
{
    public partial class Form1 : Form
    {
        public static readonly string AIRCRAFT_PATH = "Aircraft.csv";

        private readonly IDictionary<string, string> aircraftData;

        public Form1()
        {
            InitializeComponent();

            aircraftData = ReadAircraftData();

            InitComboBoxAircraft();
            InitTextBoxAircraft();
        }

        private void InitTextBoxAircraft()
        {
            textBoxAircraft.Click += (sender, e) =>
            {
                var box = (TextBox)sender;

                if (!string.IsNullOrWhiteSpace(box.Text))
                {
                    Clipboard.SetText(box.Text);

                    var tooltipTime = 500;
                    (new ToolTip()).Show("Copied", box, box.Width, 0, tooltipTime);
                }
            };
        }

        private void InitComboBoxAircraft()
        {

            comboBoxAircraft.Items.AddRange(aircraftData.Values.ToArray());

            comboBoxAircraft.KeyPress += (sender, e) =>
            {
                var box = (ComboBox)sender;
                if (box.DroppedDown == false) box.DroppedDown = true;
            };
            comboBoxAircraft.SelectedIndexChanged += (sender, e) =>
            {
                var selectedValue = ((ComboBox)sender).SelectedItem.ToString();
                var selectedValueAircraftKey = aircraftData.Where(pair => pair.Value == selectedValue).First().Key;
                textBoxAircraft.Text = selectedValueAircraftKey;
            };
        }


        private IDictionary<string, string> ReadAircraftData()
        {
            return File.ReadAllLines(AIRCRAFT_PATH)
                .Select(line => line.Split(';'))
                .SkipWhile(columns => columns[0] == "ACFT_ID")
                .OrderBy(columns => columns[1])
                .ToDictionary(columns => columns[0], columns => columns[1]);
        }

    }
}
